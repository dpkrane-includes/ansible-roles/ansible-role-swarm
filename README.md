Роль раскатывает на хостах Docker (с помощью роли [ansible-role-docker](https://gitlab.com/dpkrane-includes/ansible-roles/ansible-role-docker)) и объединяет их в swarm.

Принадлежность хостов к manager и worker нодам определяется в инвентаре.
```
swarm:
  vars:
    docker_user_add: true
    docker_pip_install: true
    docker_dcompose_install: false
  children:
    managers:
      hosts:
        instance-0:
        instance-1:
    workers:
      hosts:
        instance-2:
        instance-3:
```
